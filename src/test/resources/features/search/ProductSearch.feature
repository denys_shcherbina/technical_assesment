Feature: Search for the product

  Scenario Outline: Search product, by using correct search parameters

    When User searches for products, by using following product title: '<productTitle>'
    Then User is able to see the products list, displayed for '<productTitle>'

    Examples:
      | productTitle |
      | orange       |
      | pasta        |
      | apple        |
      | cola         |

  Scenario: Search product, by using incorrect search parameters
      When User searches for products, by using invalid product title: 'invalid'
      Then No products should be found