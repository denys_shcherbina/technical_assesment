package cucumber.steps_def.search;

import com.assesment.core.api.dto.search.ProductDTO;
import com.assesment.core.session_storage.EnvironmentVariablesSessionStorage;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import net.thucydides.core.annotations.Steps;
import org.apache.http.HttpStatus;
import org.unitils.reflectionassert.ReflectionAssert;
import serenity.steps.search.ProductsSearchApiSteps;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static org.apache.commons.lang3.StringUtils.contains;
import static org.hamcrest.Matchers.equalTo;

public class ProductsSearchStepsDef {

    @Steps
    private ProductsSearchApiSteps productsSearchApiSteps;

    @When("^User searches for products, by using following product title: '([^\"]*)'$")
    public void userSearchesForProductByTitle(final String title) {
        final List<ProductDTO> products = Arrays.asList(productsSearchApiSteps.searchForProduct(title).as(ProductDTO[].class));
        EnvironmentVariablesSessionStorage.LOOKUP_ITEMS.set(products);
    }

    @When("^User searches for products, by using invalid product title: '([^\"]*)'$")
    public void userSearchesForProductByInvalidTitle(final String invalidTitle) {
        final Response productNotFoundResponse = productsSearchApiSteps.searchForProduct(invalidTitle);
        EnvironmentVariablesSessionStorage.NO_PRODUCT_FOUND.set(productNotFoundResponse);
    }

    @Then("^User is able to see the products list, displayed for '([^\"]*)'$")
    public void userIsAbleToSeeTheProductsListDisplayedForOrange(final String title) {
        final List<ProductDTO> actualProductsList = EnvironmentVariablesSessionStorage.LOOKUP_ITEMS.get();
        final List<ProductDTO> expectedProductsList = this.filterByCondition(actualProductsList, title);
        expectedProductsList.sort(Comparator.comparing(ProductDTO::title));
        actualProductsList.sort(Comparator.comparing(ProductDTO::title));
        ReflectionAssert.assertReflectionEquals("Products are not equals.", expectedProductsList, actualProductsList);
    }

    @Then("^No products should be found$")
    public void productShouldNotBeFound() {
        final Response productNotFoundResponse = EnvironmentVariablesSessionStorage.NO_PRODUCT_FOUND.get();
        productNotFoundResponse.then()
                .assertThat()
                .statusCode(HttpStatus.SC_NOT_FOUND)
                .body("detail.message", equalTo("Not found"));
    }

    private List<ProductDTO> filterByCondition(final List<ProductDTO> productsList, final String title) {
        return productsList.stream()
                .filter(product -> {
                    final String productTitle = product.title().toLowerCase();
                    final String productUrl = product.url().toLowerCase();
                    return contains(productTitle, title) || contains(productUrl, title);
                }).collect(Collectors.toList());
    }
}
