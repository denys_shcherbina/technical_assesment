package serenity.steps.search;

import com.assesment.core.api.ProductSearchApi;
import io.restassured.response.Response;
import net.thucydides.core.annotations.Step;

public class ProductsSearchApiSteps {

    private final ProductSearchApi productSearchApi = new ProductSearchApi();

    public ProductsSearchApiSteps() {
        super();
    }

    @Step
    public Response searchForProduct(final String productTitle) {
        return productSearchApi.searchProductBy(productTitle);
    }
}
