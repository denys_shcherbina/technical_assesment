package com.assesment.core.api;

import com.assesment.core.utils.properties.PropertiesReader;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.rest.SerenityRest;

import java.util.List;
import java.util.Map;

public abstract class AbstractApi {

    private static final String API_BASE_URL = PropertiesReader.getInstance().getProperty("api.base.url");

    protected AbstractApi() {
        SerenityRest.setDefaultRequestSpecification(this.initConfig());
    }

    public Response performGetRequest(final String path, final Map<String, String> pathParams) {
        return SerenityRest.given()
                .pathParams(pathParams)
                .when()
                .get(path);
    }

    private RequestSpecification initConfig() {
        return new RequestSpecBuilder()
                .setBaseUri(API_BASE_URL)
                .addFilters(List.of(new RequestLoggingFilter(), new ResponseLoggingFilter()))
                .setContentType(ContentType.JSON)
                .build();
    }
}
