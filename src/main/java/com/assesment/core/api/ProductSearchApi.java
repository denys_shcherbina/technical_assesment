package com.assesment.core.api;

import io.restassured.response.Response;

import java.util.Map;

public class ProductSearchApi extends AbstractApi {

    private static final String PRODUCT_SEARCH_API_PATH = "/api/v1/search/demo/{product}";

    public Response searchProductBy(final String title) {
        final Map<String, String> pathParams = Map.of("product", title);
        return performGetRequest(PRODUCT_SEARCH_API_PATH, pathParams);
    }
}
