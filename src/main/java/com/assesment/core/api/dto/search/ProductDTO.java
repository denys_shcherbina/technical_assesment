package com.assesment.core.api.dto.search;

public record ProductDTO(
        String provider,
        String title,
        String url,
        String brand,
        Double price,
        String unit,
        Boolean isPromo,
        String promoDetails,
        String image
) { }
