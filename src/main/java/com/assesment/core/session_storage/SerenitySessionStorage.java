package com.assesment.core.session_storage;

import net.serenitybdd.core.Serenity;

public interface SerenitySessionStorage {

    default <T> void set(final T object) {
        Serenity.setSessionVariable(this).to(object);
    }

    default <T> T get() {
        return Serenity.sessionVariableCalled(this);
    }
}
