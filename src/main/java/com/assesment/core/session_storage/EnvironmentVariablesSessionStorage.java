package com.assesment.core.session_storage;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum EnvironmentVariablesSessionStorage implements SerenitySessionStorage {

    LOOKUP_ITEMS("${LOOKUP_ITEMS}"),
    NO_PRODUCT_FOUND("${NO_PRODUCTS}");

    private final String sessionKey;
}
