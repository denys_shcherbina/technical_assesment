package com.assesment.core.utils.properties;

import lombok.SneakyThrows;

import java.io.FileInputStream;
import java.util.Properties;

public class PropertiesReader {

    private static final String PROPERTIES_FILE_PATH = "./src/main/resources/properties/environment.properties";
    private static final Properties ENVIRONMENT_PROPERTIES = new Properties();

    private PropertiesReader() {
        init();
    }

    private static class SingletonNestedInstance {
        private static final PropertiesReader INSTANCE = new PropertiesReader();
    }

    public static PropertiesReader getInstance() {
        return SingletonNestedInstance.INSTANCE;
    }

    public String getProperty(final String key) {
        return ENVIRONMENT_PROPERTIES.getProperty(key);
    }

    @SneakyThrows
    private void init() {
        ENVIRONMENT_PROPERTIES.load(new FileInputStream(PROPERTIES_FILE_PATH));
    }
}
