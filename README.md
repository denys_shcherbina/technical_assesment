# Technical Assessment Framework

## 1. Framework stack:
| #   | Build Tool |               BDD Tool | Rest API Client Tool | Programming Language |
|:----|:----------:|-----------------------:|---------------------:|---------------------:|
| 1   |   Maven    | Cucumber, Serenity BDD |        Serenity Rest |              Java 19 |


## 1.1. Maven:
#### How to install: https://maven.apache.org/install.html

## 1.2. Java 19 (JDK 19):
#### How to install: https://docs.oracle.com/en/java/javase/11/install/installation-jdk-microsoft-windows-platforms.html#GUID-A7E27B90-A28D-4237-9383-A58B416071CA

## 2. Tests Execution:
##### 2.1. Open CMD (or Terminal) on your PC (or laptop).
##### 2.2. In CMD (or Terminal), open root directory of the framework (where the ```pom.xml``` file located).
##### 2.3. Once its opened, print the list of files in opened directory and make sure that it similar to:
```
-rw-r--r--   1 dshcherbina  staff  1205 May 17 11:40 README.md
-rw-r--r--   1 dshcherbina  staff  5364 May 16 23:10 pom.xml
drwxr-xr-x   4 dshcherbina  staff   128 Feb 10 13:39 src
drwxr-xr-x  11 dshcherbina  staff   352 May 17 00:01 target

```
##### 2.4. Execute tests, by following command:
```
mvn clean verify
```

## 3. Report Generation:
##### 3.1. After all tests are finished, report will be generated automatically and could be found in
```
./technical_assesment/target/site/serenity/index.html
```

## 4. Adding new test:
##### 4.1. Add new cucumber ```*.feature``` file to ```./test/resources/features``` folder.
##### 4.2. Write your validations in ```*.feature``` file, using ```Given```, ```When```, ```Then``` syntax.
##### 4.3. Create cucumber step definitions Java class, located in ```./test/java/cucumber.steps_def``` folder.
##### 4.4. Make sure that step definitions from your ```*.feature``` file the same as in step definitions Java class. Make them matchable between each other.

## 5. What was refactored: 
##### 5.1. ```Gradle``` build tool was replaced to ```Maven``` due to task requirements.
#### 5.2. ```serenity.conf``` file was removed, because it consisted on browser settings, which are applicable to UI tests only.
#### 5.3. Following feature file ```post_product.feature``` was renamed to ```ProductSearch.feature```, according to camel case naming rules.
#### 5.4. Was created level of abstraction (```AbstractApi.java```). There were defined basic configurations and methods for the Search API , which can be reused in child classes. Such option will help to avoid code duplications.
#### 5.5. Following file ```CarsAPI.java``` was renamed to ```ProductSearchApi.java```. It was a necessary to avoid code duplications. No need to create standalone classes, like ```CarSearchAPI.java```, ```OrangeSearchAPI.java```, ```AppleSearchAPI.java```, because we have only one ```Search API```, which can take different parameters, like product title.
#### 5.6. Added support of using ```environment.properties``` file in framework. That was done for creation a resource, where we can store common links, paths to files if any, etc. Class, which provides possibility to use ```*.properties``` file, called ```PropertiesReader.java```. It was build, by using ```Singleton``` design pattern.
#### 5.7. Following file ```TestRunner.java``` was renamed to ```AcceptanceTestSuite.java```, because it's more relabelled name for execution not only a single test, but a test suite.
#### 5.8. Was implemented possibility to use a session storage ```SerenitySessionStorage.java``` for the objects and any other entries. This class, provides possibility to store and share all the objects and entries between test steps.
##### 5.9. Feature file syntax was reworked by removing technical details and adding more correct BDD syntax
##### 5.10. Framework was build, by using design patterns, like ```singleton```, ```dependency injection```, and ```OOP``` principles, etc